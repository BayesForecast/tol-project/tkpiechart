//////////////////////////////////////////////////////////////////////////////
// FILE   : TkPieChart.tol
// PURPOSE: Package to plot a pie chart.
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
NameBlock TkPieChart =
//////////////////////////////////////////////////////////////////////////////
[[
#Require GuiTools;
  
//read only autodoc
Text _.autodoc.name = "TkPieChart";
Text _.autodoc.brief =
"Draw a pie chart on a canvas.";
Text _.autodoc.description =
"Implement a function to plot a set of values in a form of a pie chart. It is based on http://tcllib.sourceforge.net/doc/pie.html";
Text _.autodoc.url = 
"http://packages.tol-project.org/OfficialTolArchiveNetwork/repository.php";
Set _.autodoc.keys = [[ "Pie", "Chart", "Plot" ]];
Set _.autodoc.authors = [[ "josp@tol-project.org"]];
Text _.autodoc.minTolVersion = "v2.0.1 b.1";
Real _.autodoc.version.high = 0;
Real _.autodoc.version.low = 101;
Set _.autodoc.dependencies = Copy(Empty);
Set _.autodoc.nonTolResources = [[
  Text "tcl",
  Text "lib"
]] ;
Text _.autodoc.versionControl = AvoidErr.NonDecAct(OSSvnInfo("."));

#Embed "PlotFunctions.tol";

Real _is_started = False;
////////////////////////////////////////////////////////////////////////////
Real StartActions(Real void)
////////////////////////////////////////////////////////////////////////////
{
  If(_is_started, False, {
      Real _is_started := True;
      Text cwd = GetAbsolutePath( "." ) + "/";
      Real If( GuiTools::InsideTk(?), {
          Set tcl = Tcl_Eval( "namespace eval :: source tcl/init.tcl" );
          Real If( tcl["status"], {
              True
            }, {
              Error( "TkPieChart::StartActions :" + tcl["result"] );
              False
            } )
        }, {
          Warning( "TkPieChart will not plot because Tk is not available." );
          True
        } )
    })
}

]];
